#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include "map.h"
#include "error.h"

#ifdef PADAWAN

/**********************************************************************************************************/
/**********************************************************************************************************/

void map_new(unsigned width, unsigned height)
{
	map_allocate(width, height);

	for (int x=0; x<width; x++)
		map_set(x, height-1, 0); // Ground

	for (int y=0; y<height-1; y++) {
		map_set(0, y, 1); // Wall
		map_set(width-1, y, 1); // Wall
	}

	map_object_begin(9);

	// Texture pour le sol
	map_object_add("images/ground.png", 1, MAP_OBJECT_SOLID);
	// Mur
	map_object_add("images/wall.png", 1, MAP_OBJECT_SOLID);
	// Gazon
	map_object_add("images/grass.png", 1, MAP_OBJECT_SEMI_SOLID);
	// Marbre
	map_object_add("images/marble.png", 1, MAP_OBJECT_SOLID | MAP_OBJECT_DESTRUCTIBLE);
	// Herbe
	map_object_add("images/herb.png", 1, MAP_OBJECT_AIR);
	// Petit plancher flottant
	map_object_add("images/floor.png", 1, MAP_OBJECT_SEMI_SOLID);
	// Fleur
	map_object_add("images/flower.png", 1, MAP_OBJECT_AIR);
	// Piece
	map_object_add("images/coin.png", 20, MAP_OBJECT_AIR | MAP_OBJECT_COLLECTIBLE);
	// Bloc
	map_object_add("images/question.png", 17, MAP_OBJECT_SOLID | MAP_OBJECT_GENERATOR);
	
	map_set(5, height-2, 7);
	map_set(6, height-2, 6);
	map_set(7, height-2, 6);
	map_set(8, height-2, 7);
	map_set(10, height-4, 8); 

	map_object_end();
}

/**********************************************************************************************************/

void check_rw(int ret, char *str, int object) {
	if (ret <= 0) {
		if (object == -1)
			fprintf(stderr, "%s\n", str);
		else
			fprintf(stderr, "object %d: %s\n", object, str);
	}
} 

/**********************************************************************************************************/

void map_save(char *filepath)
{
	int save_fd = open(filepath, O_WRONLY | O_TRUNC | O_CREAT, 0777);
	if (save_fd == -1) {
		fprintf(stderr, "map_save open failed (%s)", filepath);
		exit(1);
	}

	int int_buffer = map_width();
	int w = write(save_fd, &int_buffer, sizeof(int));		// longueur map
	check_rw(w, "width write failed", -1);

	int_buffer = map_height();
	w = write(save_fd, &int_buffer, sizeof(int));		// hauteur map
	check_rw(w, "height write failed", -1);

	int_buffer = map_objects();
	w = write(save_fd, &int_buffer, sizeof(int));		// nombre objets
	check_rw(w, "objects nb write failed", -1);

	for (int x=0; x<map_width(); x++) {
		for (int y=0; y<map_height(); y++) {
			int_buffer = map_get(x, y);
			w = write(save_fd, &int_buffer, sizeof(int));		// type objet
			check_rw(w, "object type write failed", -1);
		}
	}

	char* string_buffer;
	for (int i=0; i<map_objects(); i++) {
		string_buffer = map_get_name(i);
		int_buffer = strlen(string_buffer);
		w = write(save_fd, &int_buffer, sizeof(int));		// taille string chemin image
		check_rw(w, "image path length write failed", i);

		w = write(save_fd, string_buffer, sizeof(char)*int_buffer);	// chemin image
		check_rw(w, "image path write failed", i);

		int_buffer = map_get_frames(i);
		w = write(save_fd, &int_buffer, sizeof(int));		// nb frames image
		check_rw(w, "frames nb write failed", i);

		int_buffer = map_get_solidity(i);
		w = write(save_fd, &int_buffer, sizeof(int));		// solidite objet
		check_rw(w, "solidity write failed", i);

		int_buffer = map_is_destructible(i);
		w = write(save_fd, &int_buffer, sizeof(int));		// objet destructible ou pas
		check_rw(w, "destructible write failed", i);

		int_buffer = map_is_collectible(i);
		w = write(save_fd, &int_buffer, sizeof(int));		// objet collectible ou pas
		check_rw(w, "collectible write failed", i);

		int_buffer = map_is_generator(i);
		w = write(save_fd, &int_buffer, sizeof(int));		// objet generateur ou pas
		check_rw(w, "generator write failed", i);
	}

	close(save_fd);
	printf("map save successful\n");
}

/**********************************************************************************************************/

void map_load(char *filepath)
{
	int save_fd = open(filepath, O_RDONLY);
	if (save_fd == -1) {
		fprintf(stderr, "map_load open failed (%s)", filepath);
		exit(1);
	}

	int int_buffer;
	int width, height, objects_nb;

	int r = read(save_fd, &width, sizeof(int));
	check_rw(r, "width read failed", -1);
	r = read(save_fd, &height, sizeof(int));
	check_rw(r, "height read failed", -1);
	map_allocate(width, height);
	r = read(save_fd, &objects_nb, sizeof(int));
	check_rw(r, "objects nb read failed", -1);

	for (int x=0; x<width; x++) {
		for (int y=0; y<height; y++) {
			r = read(save_fd, &int_buffer, sizeof(int));
			check_rw(r, "object type read failed", -1);
			map_set(x, y, int_buffer);
		}
	}

	map_object_begin(objects_nb);

	char *img_path;
	int frames_nb, solidity, destructible, collectible, generator;

	for (int i=0; i<objects_nb; i++) {
		r = read(save_fd, &int_buffer, sizeof(int));	// taille string image
		check_rw(r, "image path length write failed", i);
		img_path = calloc(int_buffer, sizeof(char));

		r = read(save_fd, img_path, sizeof(char)*int_buffer);
		check_rw(r, "image path read failed", i);
		r = read(save_fd, &frames_nb, sizeof(int));
		check_rw(r, "frames nb read failed", i);
		r = read(save_fd, &solidity, sizeof(int));
		check_rw(r, "solidity read failed", i);
		r = read(save_fd, &destructible, sizeof(int));
		check_rw(r, "destructible read failed", i);
		r = read(save_fd, &collectible, sizeof(int));
		check_rw(r, "collectible read failed", i);
		r = read(save_fd, &generator, sizeof(int));
		check_rw(r, "generator read failed", i);

		if (collectible) {
			if (destructible)
				map_object_add(img_path, frames_nb, MAP_OBJECT_COLLECTIBLE | MAP_OBJECT_DESTRUCTIBLE);
			else
				map_object_add(img_path, frames_nb, MAP_OBJECT_COLLECTIBLE);
		}
		else if(generator) {
			if (destructible)
				map_object_add(img_path, frames_nb, solidity | MAP_OBJECT_GENERATOR | MAP_OBJECT_DESTRUCTIBLE);
			else
				map_object_add(img_path, frames_nb, solidity | MAP_OBJECT_GENERATOR);
		}
		else {
			if (destructible)
				map_object_add(img_path, frames_nb, solidity | MAP_OBJECT_DESTRUCTIBLE);
			else 
				map_object_add(img_path, frames_nb, solidity);
		}

		free(img_path);
	}

	map_object_end();

	close(save_fd);
	printf("map loading successful\n");
}
	

#endif
