#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>
#include <signal.h>
#include <pthread.h>

#include "timer.h"

// Return number of elapsed µsec since... a long time ago
static unsigned long get_time (void)
{
  struct timeval tv;

  gettimeofday (&tv ,NULL);

  // Only count seconds since beginning of 2016 (not jan 1st, 1970)
  tv.tv_sec -= 3600UL * 24 * 365 * 46;
  
  return tv.tv_sec * 1000000UL + tv.tv_usec;
}

struct timeval get_delay(Uint32 delay)
{
    struct timeval tv;
    tv.tv_sec = delay/1000;
    tv.tv_usec = (delay%1000)*1000;
    return tv;
}

struct timeval now(struct timeval delay)
{
    struct timeval now;
    gettimeofday(&now, NULL);
    timeradd(&now, &delay, &now);
    return now;
}

struct event_t
{
    void* param;
    struct timeval time;
};

typedef struct event_t event;

int comp(const void* a, const void* b)
{
    event ea = *(event*) a;
    event eb = *(event*) b;

    if (timercmp(&ea.time, &eb.time, <)) return -1;
    if (timercmp(&ea.time, &eb.time, ==)) return 0;
    if (timercmp(&ea.time, &eb.time, >)) return 1;
}

// GLOBAL
int N = 0;
event E[100];
pthread_mutex_t m = PTHREAD_MUTEX_INITIALIZER;

void push(void* param, struct timeval time)
{
    pthread_mutex_lock(&m);
    E[N].param = param;
    E[N].time = time;
    N++;
    qsort(E, (size_t) N, sizeof(event), comp);
    pthread_mutex_unlock(&m);
}

int pop(event* e)
{
    if(N == 0) return 0;

    struct timeval now;
    gettimeofday(&now, NULL);

    if(timercmp(&E[0].time, &now, <=))
    {
        pthread_mutex_lock(&m);
        *e = E[0];
        N--;
        E[0] = E[N];
        qsort(E, (size_t) N, sizeof(event), comp);
        pthread_mutex_unlock(&m);
        return 1;
    }
    else
    {
        return 0;
    }
}

void ding(int s)
{
    event e;

    // Remove & send events.
    while(pop(&e))
    {
        sdl_push_event(e.param);
    }

    // Re-signal
    if(N > 0)
    {
        struct timeval tv;
        timersub(&E[0].time, &e.time, &tv);
        struct itimerval itv;
        itv.it_interval.tv_sec = 0;
        itv.it_interval.tv_usec = 0;
        itv.it_value = tv;
        setitimer(ITIMER_REAL, &itv, NULL);
    }
}

void* timer(void* ptr)
{
    struct sigaction s;
    s.sa_handler = ding;
    sigemptyset(&s.sa_mask);

    sigaction(SIGALRM, &s, NULL);

    sigset_t m;
    sigemptyset(&m);

    while(1) sigsuspend(&m);

    return NULL;
}

#ifdef PADAWAN

// timer_init returns 1 if timers are fully implemented, 0 otherwise
int timer_init (void)
{
    pthread_t daemon;
    pthread_create(&daemon, NULL, timer, NULL);

    return 1; // Implementation ready
}

timer_id_t timer_set (Uint32 delay, void *param)
{
    struct timeval tv = get_delay(delay);

    struct itimerval itv;
    itv.it_interval.tv_sec = 0;
    itv.it_interval.tv_usec = 0;
    itv.it_value = tv;

    struct timeval time = now(tv);

    if(N == 0) setitimer(ITIMER_REAL, &itv, NULL);
    else
    {
        // Only signals the nearest.
        if(timercmp(&time, &E[0].time, <))
        {
            setitimer(ITIMER_REAL, &itv, NULL);
        }
    }

    // Add event.
    push(param, time);

    return (timer_id_t) N-1;
}

int timer_cancel (timer_id_t timer_id)
{
    // Remove the event.
    // But ignore the signal
    // It's useless to disable the signal.

    if(N > timer_id)
    {
        N--;
        E[timer_id] = E[N];
        qsort(E, (size_t) N, sizeof(event), comp);
        return 1;
    }
    return 0;
}

#endif
