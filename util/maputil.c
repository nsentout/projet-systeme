#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include "../include/map.h"

/**********************************************************************************************************/
/**********************************************************************************************************/

void get_width(int fd) {
	int width;
	read(fd, &width, sizeof(int));
	printf("Width is %d\n", width);
}

/**********************************************************************************************************/

void get_height(int fd) {
	lseek(fd, sizeof(int), SEEK_SET);
	int height;
	read(fd, &height, sizeof(int));
	printf("Height is %d\n", height);
}

/**********************************************************************************************************/

void get_objects(int fd) {
	lseek(fd, 2*sizeof(int), SEEK_SET);
	int objects_nb;
	read(fd, &objects_nb, sizeof(int));
	printf("There are %d objects in the map\n", objects_nb);
}

/**********************************************************************************************************/

void get_info(int fd) {
	get_width(fd);
	get_height(fd);
	get_objects(fd);
}

/**********************************************************************************************************/

void set_width(int fd, int new_width, char *saved_map_path) {
	if (new_width >= MAP_MIN_WIDTH && new_width <= MAP_MAX_WIDTH) {		// si la longueur demandee est correcte
		int old_width, height, objects_nb;
		
		read(fd, &old_width, sizeof(int));			// lit l'ancienne longueur
		if (new_width == old_width) {
			printf("This is already the map width!\n");
			return;
		}
		read(fd, &height, sizeof(int));				// lit la hauteur
		read(fd, &objects_nb, sizeof(int));			// lit le nb d'objets

		int tmp_fd = open("tmp", O_WRONLY | O_TRUNC | O_CREAT, 0777);	// fichier temporaire qui remplacera le fichier saved_map
		write(tmp_fd, &new_width, sizeof(int));		// ecrit la nouvelle longueur dans le fichier tmp
		write(tmp_fd, &height, sizeof(int));		// ecrit la hauteur dans le fichier tmp
		write(tmp_fd, &objects_nb, sizeof(int));	// ecrit le nb d'objets dans le fichier tmp

		int buf;
		int old_objects_added=0;

		// si on reduit la longueur de la map
		if (old_width > new_width) {
			int objects_to_keep = old_width*height-height*(old_width-new_width);	// nb d'objets a garder sur la nouvelle map
			
			while (old_objects_added < objects_to_keep && read(fd, &buf, sizeof(int)) > 0) {  // copie tous les objets de la map sauf ceux a effacer
				write(tmp_fd, &buf, sizeof(int));	
				old_objects_added++;
			}
		
			lseek(fd, sizeof(int)*(height*(old_width-new_width)), SEEK_CUR);		// saute les objets qui vont etre supprimes
		}

		// si on augmente la longueur de la map
		else if (old_width < new_width) {
			int data_to_keep = old_width*height;		// nb de donnees a lire avant d'ajouter des nouvelles donnees

			while (old_objects_added < data_to_keep && read(fd, &buf, sizeof(int)) > 0) {	// copie le fichier jusqu'a arriver a la fin des objets
				write(tmp_fd, &buf, sizeof(int));
				old_objects_added++;
			}

			int objects_to_add = height*(new_width-old_width);		// nb d'objets a ajouter pour combler la nouvelle map
			buf = -1;

			while (objects_to_add > 0) {				// ajoute les objets a la nouvelle map
				write(tmp_fd, &buf, sizeof(int));
				objects_to_add--;
			}
		}

		char *img_path;								// finit de copier le fichier d'origine
		for (int i=0; i<objects_nb; i++)  {
			read(fd, &buf, sizeof(int));				// lit la taille du string du nom de l'image
			write(tmp_fd, &buf, sizeof(int));			// l'ecrit dans le fichier tmp
			img_path = calloc(buf, sizeof(char));
			read(fd, img_path, sizeof(char)*buf);		// lit le chemin vers l'image
			write(tmp_fd, img_path, sizeof(char)*buf);	// l'écrit dans le fichier tmp
			for (int j=0; j<5; j++) {
				read(fd, &buf, sizeof(int));			// lit le reste des infos de l'objet
				write(tmp_fd, &buf, sizeof(int));		// ecrit le reste des infos de l'objet
			}
			free(img_path);
		}

		close(tmp_fd);
		close(fd);
		printf("width change successful\n");
		execlp("mv", "mv", "tmp", saved_map_path, NULL);
	}

	printf("width must be in the range [%d,%d]\n", MAP_MIN_WIDTH, MAP_MAX_WIDTH);
}

/**********************************************************************************************************/

void set_height(int fd, int new_height, char *saved_map_path) {
	if (new_height >= MAP_MIN_HEIGHT && new_height <= MAP_MAX_HEIGHT) {		// si la hauteur demandee est correcte
		int width, old_height, objects_nb;
		
		read(fd, &width, sizeof(int));				// lit la longueur
		read(fd, &old_height, sizeof(int));			// lit l'ancienne hauteur
		if (new_height == old_height) {
			printf("This is already the map height!\n");
			return;
		}
		read(fd, &objects_nb, sizeof(int));			// lit le nb d'objets

		int tmp_fd = open("tmp", O_WRONLY | O_TRUNC | O_CREAT, 0777);	// fichier temporaire qui remplacera le fichier saved_map
		write(tmp_fd, &width, sizeof(int));			// ecrit la longueur dans le fichier tmp
		write(tmp_fd, &new_height, sizeof(int));	// ecrit la nouvelle hauteur dans le fichier tmp
		write(tmp_fd, &objects_nb, sizeof(int));	// ecrit le nb d'objets dans le fichier tmp

		int buf;
		int old_objects_added = 0;				// objets de l'ancienne map qui seront conserves
		int cpt_width = 0;

		// si on reduit la hauteur de la map
		if (old_height > new_height) {
			while (cpt_width < width) {		// parcours le fichier saved_map jusqu'aux donnees des images
				lseek(fd, sizeof(int)*(old_height-new_height), SEEK_CUR);	// saute les objets a supprimer

				while (old_objects_added < new_height) {					// copie tous les objets de la map a garder
					read(fd, &buf, sizeof(int));
					write(tmp_fd, &buf, sizeof(int));	
					old_objects_added++;
				}

				old_objects_added = 0;
				cpt_width++;
			}
		}

		// si on augmente la hauteur de la map
		else if (old_height < new_height) {
			int new_objects_added = 0;

			while (cpt_width < width) {		// parcours le fichier saved_map jusqu'aux donnees des images
				buf = -1;

				while (new_objects_added < new_height-old_height) {		// ajoute les nouveaux objets
					write(tmp_fd, &buf, sizeof(int));
					new_objects_added++;
				}

				while (old_objects_added < old_height) {				// ajoute les anciens objets
					read(fd, &buf, sizeof(int));
					write(tmp_fd, &buf, sizeof(int));
					old_objects_added++;
				}

				new_objects_added = 0;
				old_objects_added = 0;
				cpt_width++;
			}
		}

		char *img_path;								// finit de copier le fichier d'origine
		for (int i=0; i<objects_nb; i++)  {
			read(fd, &buf, sizeof(int));				// lit la taille du string du nom de l'image
			write(tmp_fd, &buf, sizeof(int));			// l'ecrit dans le fichier tmp
			img_path = calloc(buf, sizeof(char));
			read(fd, img_path, sizeof(char)*buf);		// lit le chemin vers l'image
			write(tmp_fd, img_path, sizeof(char)*buf);	// l'écrit dans le fichier tmp
			for (int j=0; j<5; j++) {
				read(fd, &buf, sizeof(int));			// lit le reste des infos de l'objet
				write(tmp_fd, &buf, sizeof(int));		// ecrit le reste des infos de l'objet
			}
			free(img_path);
		}

		close(tmp_fd);
		close(fd);
		printf("height change successful\n");
		execlp("mv", "mv", "tmp", saved_map_path, NULL);
	}

	printf("height must be in the range [%d,%d]\n", MAP_MIN_HEIGHT, MAP_MAX_HEIGHT);
}

/**********************************************************************************************************/
/**********************************************************************************************************/

void write_string(int fd, char* str)
{
	int s = strlen(str);
	write(fd, &s, sizeof(int));
	write(fd, str, sizeof(char)*s);
}

int numerized(char* property)
{
	if(strcmp(property, "air") == 0) return 0;
	else if(strcmp(property, "solid") == 0) return 2;
	else if(strcmp(property, "semi-solid") == 0) return 1;
	else if(strcmp(property, "destructible") == 0) return 1;
	else if(strcmp(property, "collectible") == 0) return 1;
	else if(strcmp(property, "generator") == 0) return 1;
	else if(strcmp(property, "not-destructible") == 0) return 0;
	else if(strcmp(property, "not-collectible") == 0) return 0;
	else if(strcmp(property, "not-generator") == 0) return 0;
	else return -1;
}

void parse_object(int fd, int argc, char** argv)
{
	int w, h, n;
	read(fd, &w, sizeof(int));
	read(fd, &h, sizeof(int));
	read(fd, &n, sizeof(int));
	lseek(fd, (w*h)*sizeof(int), SEEK_CUR);

	if((argc - 3)%6 == 0)
	{
		int m = argc/6;
		if(m < n)
		{
			printf("ERROR : You need more objects to use setobjects (%d/%d).\n",m, n);
			exit(1);
		}

		int p;
		for(int i=3; i < argc; i+=6)
		{
			write_string(fd, argv[i]);

			p = atoi(argv[i+1]);
			write(fd, &p, sizeof(int));

			for(int t=2; t < 6; t++)
			{
				p = numerized(argv[i+t]);
				if(p < 0)
				{
					printf("WARNING : Can't understand property '%s', set to default.\n", argv[i+t]);
					p = 0;
				}
				write(fd, &p, sizeof(int));
			}
		}

		lseek(fd, 2*sizeof(int), SEEK_SET);
		write(fd, &m, sizeof(int));
		printf("objects change successful\n");
	}
	else
	{
		printf("Incorrect arguments\n");
		printf("Try maputil <file> --setobjects <filename> <frames> <solidity> <destructible> <collectible> <generator>\n");
		exit(1);
	}
}

/**********************************************************************************************************/
/**********************************************************************************************************/

struct object_t
{
	char filename[1024];
	int frames;
	int solidity;
	int destructible;
	int collectible;
	int generator;
	struct object_t* next;
};

typedef struct object_t* object;

// WARNING : SET the cursor before !
object load_object(int fd, int* t, int* n)
{
	object head = NULL;
	object prev, obj;

	int s, m = 0;
	for(int i=0; i < *n; i++)
	{
		read(fd, &s, sizeof(int));

		if(t[i])
		{
			obj = malloc(sizeof(object));
			obj->next = NULL;
		
			read(fd, &obj->filename, s);

			read(fd, &obj->frames, sizeof(int));
			read(fd, &obj->solidity, sizeof(int));
			read(fd, &obj->destructible, sizeof(int));
			read(fd, &obj->collectible, sizeof(int));
			read(fd, &obj->generator, sizeof(int));

			if(head == NULL)
			{
				head = obj;
				prev = obj;
			}
			else
			{
				prev->next = obj;
				prev = obj;
			}

			t[i] = m;
			m+= 1;
		}
		else
		{
			lseek(fd, s*sizeof(char)+5*sizeof(int), SEEK_CUR);
		}
	}

	*n = m;

	return head;
}

// WARNING : SET the cursor before !
int save_object(object h, int fd)
{
	int s, sum = 0;
	object obj = h;
	while(obj != NULL)
	{
		s = strlen(obj->filename);
		write(fd, &s, sizeof(int));
		write(fd, obj->filename, sizeof(char)*s);
		sum+= (s*sizeof(char));
		write(fd, &obj->frames, sizeof(int));
		write(fd, &obj->solidity, sizeof(int));
		write(fd, &obj->destructible, sizeof(int));
		write(fd, &obj->collectible, sizeof(int));
		write(fd, &obj->generator, sizeof(int));
		sum+= (sizeof(int)*5);

		obj = obj->next;
	}
	return sum;
}

void free_object(object t)
{
	object h;
	while(t != NULL)
	{
		h = t;
		t = t->next;
		free(h);
	}
}

void purge(int fd)
{
	int w, h, n;
	read(fd, &w, sizeof(int));
	read(fd, &h, sizeof(int));
	read(fd, &n, sizeof(int));

	// Total size
	off_t trunc = (sizeof(int)*3);

	// Array of marked objects
	int* ts = calloc(n, sizeof(int));

	// Search & mark occurrence
	int t;
	for(int i=0; i < (w*h); i++)
	{
		read(fd, &t, sizeof(int));
		if(t >= 0) ts[t] = 1;
	}

	// Read only the marked objects
	object objs = load_object(fd, ts, &n);

	// Rewind the file
	lseek(fd, 2*sizeof(int), SEEK_SET);

	// Write the number of used objects
	write(fd, &n, sizeof(int));
	
	// Correct the index of objects
	for(int i=0; i < (w*h); i++)
	{
		read(fd, &t, sizeof(int));
		if(t >= 0)
		{
			lseek(fd, -sizeof(int), SEEK_CUR);
			t = ts[t];
			write(fd, &t, sizeof(int));
		}
	}

	trunc+= (w*h*sizeof(int));

	// Write the objects
	trunc+= save_object(objs, fd);

	// Free the objects list
	free_object(objs);
	free(ts);

	// Truncate the file
	trunc+= 100;
	ftruncate(fd, trunc);

	printf("pruning successful\n");
}

/**********************************************************************************************************/
/**********************************************************************************************************/

int main(int argc, char** argv) {
	if (argc < 3) {
		printf("Incorrect arguments\n");
		printf("Try maputil <file> --option\n");
		exit(1);
	}

	int map_fd = open(argv[1], O_RDWR);
	if (map_fd == -1) {
		fprintf(stderr, "open failed (%s)", argv[1]);
		exit(1);
	}

	if (strcmp(argv[2], "--getwidth") == 0) {
		get_width(map_fd);
	}
	else if (strcmp(argv[2], "--getheight") == 0) {
		get_height(map_fd);
	}
	else if (strcmp(argv[2], "--getobjects") == 0) {
		get_objects(map_fd);
	}
	else if (strcmp(argv[2], "--getinfo") == 0) {
		get_info(map_fd);
	}
	else if (strcmp(argv[2], "--setwidth") == 0) {
		if (argc < 4) {
			printf("Incorrect arguments\n");
			printf("Try maputil <file> --setwidth <w>\n");
			exit(1);
		}
		set_width(map_fd, atoi(argv[3]), argv[1]);	
	}
	else if (strcmp(argv[2], "--setheight") == 0) {
		if (argc < 4) {
			printf("Incorrect arguments\n");
			printf("Try maputil <file> --setheight <h>\n");
			exit(1);
		}
		set_height(map_fd, atoi(argv[3]), argv[1]);
	}
	else if (strcmp(argv[2], "--setobjects") == 0)
	{
		parse_object(map_fd, argc, argv);
	}
	else if (strcmp(argv[2], "--pruneobjects") == 0)
	{
		purge(map_fd);
	}
	else
	{
		printf("Incorrect arguments\n");
		printf("Try maputil <file> --option\n");
		exit(1);
	}

	if (map_fd != -1)	// si map_fd n'a pas deja ete ferme
		close(map_fd);

	return 0;
}
